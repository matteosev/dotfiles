#!/bin/sh
# script succesfully tested with bash and dash
# check  README.md before using it

# create a backup directory based on the current date and time
backup=$HOME/dotfiles_backup_$(date +%F\_%T)
mkdir $backup 

verbose=true
ask=false

for arg in "$@"
do
	case $arg in
		"-q") verbose=false ;;
		"-a") ask=true ;;
	esac
done

# ask mode
if  [ $ask = true ]; then 
	echo "Choose the files you want to install (y / n) :"
	echo "----------------------------------------------"
	for file in $(ls home)
	do
		install=""
		if [ -d home/$file ]; then
			file=$file/
		fi
		# while user input not correct, ask for installation
		while [ "$install" != "y"  -a  "$install" != "n" ]
		do
			# if file is directory, show what's inside
			if [ -d home/$file -a "$verbose" = true ]; then
				echo "Files to install in $HOME/.$file :"
				for subfile in $(ls home/$file)
				do
					echo "\t$subfile"
				done
			fi
			read -p ".$file <- " install
		done
		if [ "$install" = "y" ]; then
			# if existent file in user HOME, back it up
			if [ -e $HOME/.$file ]; then
				if [ -d home/$file ]; then
					cp -r $HOME/.$file $backup
				else
					mv $HOME/.$file $backup
				fi
			fi
			# put new dotfile in user HOME
			cp -RT home/$file $HOME/.$file
		fi
	done
# no ask mode
else
	if [ $verbose = true ]; then
		echo "Following files are being installed in $HOME :"
		echo "--------------------------------------"
	fi
	for file in $(ls home)
	do
		if [ -d home/$file ]; then
			file=$file/
			# if existent directory in user HOME, backup but keep it
			if [ -e $HOME/.$file ]; then
				cp -r $HOME/.$file $backup
			fi
			# merge existent dir or create it
			cp -RT home/$file $HOME/.$file
		else
			# if existent file in user HOME, back it up, and remove it
			if [ -e $HOME/.$file ]; then
				mv $HOME/.$file $backup
			fi
			# put new dotfile in user HOME
			cp home/$file  $HOME/.$file
		fi
		if [ $verbose = true ]; then
			echo ".$file"
		fi
	done
fi

if [ $verbose = true ]; then
	echo "--------------------------------------"
	echo "Successfull installation"
	echo "A backup of your dotfiles can be found here: $backup"
fi

