#!/bin/sh

# Check which dotfiles are installed


echo "Installed"
for file in $(ls -p home | grep -v /)
do
	if [ "$(cmp home/$file $HOME/.$file)" = "" ]; then
		echo "\t$file"
	fi
done

echo "Not installed"
for file in $(ls -p home | grep -v /)
do
	if [ "$(cmp home/$file $HOME/.$file)" != "" ]; then
		echo "\t$file"
	fi
done

echo "Not known"
for file in $(ls -p home | grep /)
do
		echo "\t$file"
done
