# dotfiles

*Dotfiles* are files whose name begins with a *.*, these files are often used to configure the default behavior of some softwares. They are hidden by default so you can't see them with a regular `ls` or by opening your file manager.

## Installation

You can do it manually by downloading the files one by one and if you want modify each file to suit your needs or use the *install.sh* script.

### How to

1. clone repository
```sh
git clone https://github.com/matteosev/dotfiles
```
2. cd in repository
```sh
cd dotfiles
```
3. give installation script the right rights
```sh
chmod +x install.sh
```
4. run installation script
```sh
./install.sh
```

### Full install

*Requirements*
 
* No display manager
* Xorg 
* bash 
* vim
* feh (for wallpaper)
* sxhkd (for shortcuts)
* picom (compositor)
* numlockx

That's the type of install by default when you run *install.sh*, not recommended.

### Partial install

Follow the *How to* and when you're at the 4th stage you can run
```sh
./install.sh -a
```
to make the script ask you which file you want to install, one by one.
I recommend you to do it like this and then tweak the files to make them work for your setup.

### Pick what you want 

Take a look at the content of the files and if you like something, add it to your own dotfiles.

## scripts

Don't forget to run 
```sh
chmod +x <script>
```
before executing a script.

These scripts habe been succesfully tested with *bash* and *dash*.
To see what is your default shell run (assuming you are on Linux)
```sh
ls -l /bin/sh
```
You should see something like this
```sh
lrwxrwxrwx 1 root root 4 May 27 09:25 /bin/sh -> dash
```
which in my case means /bin/sh is a link to dash, so my default shell is dash.

By default the scripts use /bin/sh so if your shell doesn't run the scripts, try to change the shebangs to something like this 
```sh
/bin/dash
# or
/bin/bash
```
depending on the shell available on your system.

### install.sh

Installation script.

Usage:
```sh
./install.sh 		# full verbose install
./install.sh -q		# full quiet install
./install.sh -a 	# partial verbose install (choose the files you want to install)
./install.sh -a -q 	# partial quiet install
```

### status.sh

Check which dotfiles are installed.
Doesn't work for dotfiles which are directories.
