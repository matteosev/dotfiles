syntax on
autocmd vimenter * colorscheme gruvbox
set background=dark
set number relativenumber

" filetype based indentation
filetype plugin indent on
" automatic indenting
set smartindent
" size of tab when >> or << pressed
set shiftwidth=4
" size of tab in insert mode
set tabstop=4
"execute the search as you type
set incsearch
" case insensitive search
set ignorecase
" don't wrap long lines
set nowrap
" always show status bar
set laststatus=2
" format of the status bar
set statusline=%#GruvboxBlueSign#\ %{mode()}%F\ %h%m%r%=\ %P\ 
" number of column to scroll when going off the screen
set sidescroll=1
" minimum distance between the cursor and the border of the screen
set sidescrolloff=0

" some personal keybindings
let mapleader = ','
let maplocalleader = ','
inoremap <c-d> <esc>ddi
inoremap <c-u> <esc>viwUi
nnoremap <leader>u <esc>viwU
:nnoremap <leader>ev :vsplit $MYVIMRC<cr>
:nnoremap <leader>sv :source $MYVIMRC<cr>

" disable arrow keys
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>

" toggle relative numbers between windows
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

" jump to last visited line when reopen file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif
